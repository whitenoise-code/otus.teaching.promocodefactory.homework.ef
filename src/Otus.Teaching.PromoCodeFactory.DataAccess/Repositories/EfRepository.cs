﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> 
        where T: BaseEntity
    {
        private readonly DbContext _context;

        public EfRepository(DbContext context)
        {
            _context = context;
            //_context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            var items = _context.Set<T>().AsEnumerable();
            return Task.FromResult(items);
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var item = await _context.Set<T>()
                .FirstOrDefaultAsync(entity => entity.Id == id);
            return item;
        }

        public Task<IEnumerable<T>> GetByIdsAsync(IEnumerable<Guid> ids)
        {
            var items = _context.Set<T>().Where(entity => ids.Contains(entity.Id)).AsEnumerable();
            return Task.FromResult(items);
        }

        public async Task AddAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(T entity)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
