﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomersRepository : IRepository<Customer>
    {
        private readonly PromocodesContext _ctx;
        public CustomersRepository(PromocodesContext context)
        {
            _ctx = context;
            //_ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<IEnumerable<Customer>> GetAllFullAsync()
        {
            var customers = await _ctx.Customers
                .Include(customer => customer.Promocodes)
                .Include(customer => customer.CustomerPreferences)
                .ThenInclude(preference => preference.Preference).AsNoTracking()
                .ToListAsync();

            return customers;
        }

        public async Task<Customer> GetFullAsync(Guid id)
        {
            var customer = await _ctx.Customers
                .Include(customer => customer.Promocodes)
                .Include(customer => customer.CustomerPreferences)
                .ThenInclude(preference => preference.Preference)
                .SingleOrDefaultAsync(customer => customer.Id == id);

            return customer;
        }

        public Task<IEnumerable<Customer>> GetAllAsync()
        {
            return Task.FromResult(_ctx.Customers.AsEnumerable());
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _ctx.Customers.SingleOrDefaultAsync(customer => customer.Id == id);
        }

        public Task<IEnumerable<Customer>> GetByIdsAsync(IEnumerable<Guid> ids)
        {
            var items = _ctx.Customers.Where(customer => ids.Contains(customer.Id)).AsEnumerable();
            return Task.FromResult(items);
        }

        public async Task AddAsync(Customer entity)
        {
            await _ctx.AddAsync(entity);
            await _ctx.SaveChangesAsync();
        }

        public async Task UpdateAsync(Customer entity)
        {
            _ctx.Update(entity);
            await _ctx.SaveChangesAsync();
        }

        public async Task RemoveAsync(Customer entity)
        {
            _ctx.Remove(entity);
            await _ctx.SaveChangesAsync();
        }
    }
}
