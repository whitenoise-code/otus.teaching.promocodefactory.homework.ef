﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.TypeConfigurations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromocodesContext : DbContext
    {
        public PromocodesContext(DbContextOptions<PromocodesContext> options) : base(options)
        {
        }
        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Promocode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreference { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PromocodeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerPreferenceEntityTypeConfiguration());
        }
    }
}
