﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.TypeConfigurations
{
    class PromocodeEntityTypeConfiguration : IEntityTypeConfiguration<Promocode>
    {
        public void Configure(EntityTypeBuilder<Promocode> builder)
        {
            builder.Property(p => p.PromoCode)
                .HasMaxLength(200);
            builder.Property(p => p.PartnerName)
                .HasMaxLength(100);
            builder.Property(p => p.ServiceInfo)
                .HasMaxLength(300);
            builder.HasOne(p=>p.Customer)
                .WithMany(customer => customer.Promocodes)
                .IsRequired();
        }
    }
}
