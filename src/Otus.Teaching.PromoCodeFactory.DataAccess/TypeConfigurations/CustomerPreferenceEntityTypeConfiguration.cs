﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.TypeConfigurations
{
    class CustomerPreferenceEntityTypeConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            builder
                .HasOne(cp=>cp.Customer)
                .WithMany(customer => customer.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            builder.HasOne(cp=>cp.Preference)
                .WithMany(p=>p.CustomerPreferences)
                .HasForeignKey(cp=> cp.PreferenceId);
        }
    }
}
