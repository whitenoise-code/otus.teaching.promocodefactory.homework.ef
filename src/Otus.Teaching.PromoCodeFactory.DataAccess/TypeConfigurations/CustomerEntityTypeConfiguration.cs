﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.TypeConfigurations
{
    class CustomerEntityTypeConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(p => p.Id)
                .ValueGeneratedNever();
            builder.Property(p => p.Email)
                .HasMaxLength(100);
            builder.Property(p => p.FirstName)
                .HasMaxLength(100);
            builder.Property(p => p.LastName)
                .HasMaxLength(100);
        }
    }
}
