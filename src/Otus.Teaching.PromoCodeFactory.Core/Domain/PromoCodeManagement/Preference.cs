﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualBasic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }

        public Collection<CustomerPreference> CustomerPreferences { get; set; }
    }
}