﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Promocode> _promocodesRepository;
        private readonly CustomersRepository _customersRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferencesRepository;

        public PromocodesController(IRepository<Promocode> promocodesRepository,
            CustomersRepository customersRepository,
            IRepository<Employee> employeeRepository,
            IRepository<Preference> preferencesRepository)
        {
            _promocodesRepository = promocodesRepository;
            _customersRepository = customersRepository;
            _employeeRepository = employeeRepository;
            _preferencesRepository = preferencesRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodesRepository.GetAllAsync();
            var response = promocodes.Select(promocode => promocode.ToShortPromocodeResponse());
            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferencesRepository.GetByIdAsync(Guid.Parse(request.Preference));
            if (preference ==null)
            {
                return BadRequest();
            }
            
            var employees = await _employeeRepository.GetAllAsync();
            var emp = employees
                .ToList()
                .Find(employee => employee.FullName == request.PartnerName);
            
            if (emp == null)
            {
                return BadRequest();
            }

            var promocode =  new Promocode
            {
                BeginDate = DateTime.ParseExact(request.BeginDate, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                EndDate = DateTime.ParseExact(request.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                PromoCode = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PreferenceId = preference.Id,
                PartnerManager = emp,
                PartnerName = emp.FullName,
            };

            await GivePromocodeToCustomers(promocode);

            return Accepted();
        }

        private async Task GivePromocodeToCustomers(Promocode promocode)
        {
            var customers = await _customersRepository.GetAllFullAsync();

            var customersWithPref = customers
                .SelectMany(customer => customer.CustomerPreferences)
                .Where(cp => cp.PreferenceId == promocode.PreferenceId)
                .Select(cp=>cp.Customer)
                .First();

            promocode.CustomerId = customersWithPref.Id;

            await _promocodesRepository.AddAsync(promocode);


        }
    }
}