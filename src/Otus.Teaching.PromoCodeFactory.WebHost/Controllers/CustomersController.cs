﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;
        private readonly CustomersRepository _customersRepository;
        private readonly IRepository<Promocode> _promocodsRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> repository,
            CustomersRepository customersRepository,
            IRepository<Promocode> promocodsRepository,
            IRepository<Preference> preferenceRepository)
        {
            _repository = repository;
            _customersRepository = customersRepository;
            _promocodsRepository = promocodsRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Получить список всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<CustomerShortResponse>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();
            var response = customers
                .Select(customer => customer.ToShortResponse());
            return Ok(response);
        }
        
        /// <summary>
        /// Получить покупателя по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор покупателя</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetFullAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            var response = customer.ToCustomerResponse();

            return Ok(response);
        }
        
        /// <summary>
        /// Создеть нового покупателя
        /// </summary>
        /// <param name="request">Запрос для создания покупателя</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = CustomerFactory.CreateFromRequest(request);
            await _customersRepository.AddAsync(customer);
            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, null);
        }
        
        /// <summary>
        /// Редактировать покупателя
        /// </summary>
        /// <param name="id">Идентификатор покупателя</param>
        /// <param name="request">Запрос для редактирования покупателя</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            customer.EditFromRequest(request);
            await _customersRepository.UpdateAsync(customer);
            return Ok();
        }
        
        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <param name="id">Идентификатор покупателя</param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var c = await _customersRepository.GetByIdAsync(id);
            if (c is null)
            {
                return NotFound();
            }

            await _customersRepository.RemoveAsync(new Customer {Id = id});

            return NoContent();
        }
    }
}