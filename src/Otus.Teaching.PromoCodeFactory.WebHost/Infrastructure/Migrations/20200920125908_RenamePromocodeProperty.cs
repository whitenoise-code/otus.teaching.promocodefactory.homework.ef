﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure.Migrations
{
    public partial class RenamePromocodeProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "PromoCodes");

            migrationBuilder.AddColumn<string>(
                name: "PromoCode",
                table: "PromoCodes",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromoCode",
                table: "PromoCodes");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "PromoCodes",
                type: "TEXT",
                maxLength: 200,
                nullable: true);
        }
    }
}
