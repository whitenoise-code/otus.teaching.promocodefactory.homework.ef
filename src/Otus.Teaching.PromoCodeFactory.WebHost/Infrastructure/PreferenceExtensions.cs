﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public static class PreferenceExtensions
    {
        public static PreferenceResponse ToPreferenceResponse(this Preference p)
        {
            return new PreferenceResponse
            {
                Id = p.Id,
                Name = p.Name
            };
        }
    }
}
