﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public class PromocodesDbContextSeeder
    {
        public static void Seed(PromocodesContext context)
        {
            if (!context.Preferences.Any())
            {
                context.Preferences.AddRange(FakeDataFactory.Preferences);
            }

            if (!context.Customers.Any())
            {
                context.Customers.AddRange(FakeDataFactory.Customers);
            }

            context.SaveChanges();
        }
    }
}
