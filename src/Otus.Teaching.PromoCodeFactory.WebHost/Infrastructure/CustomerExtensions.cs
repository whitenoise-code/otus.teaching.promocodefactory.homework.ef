﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public static class CustomerExtensions
    {
        public static CustomerShortResponse ToShortResponse(this Customer cust)
        {
            return new CustomerShortResponse
            {
                Id = cust.Id,
                Email = cust.Email,
                FirstName = cust.FirstName,
                LastName = cust.LastName
            };
        }

        public static CustomerResponse ToCustomerResponse(this Customer c)
        {
            return new CustomerResponse
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                PromoCodes = c.Promocodes
                    .Select(p => p.ToShortPromocodeResponse())
                    .ToList(),
                Preferences = c.CustomerPreferences
                    .Select(preference => preference.Preference.ToPreferenceResponse())
                    .ToList()

            };
        }

        public static void EditFromRequest(this Customer customer, CreateOrEditCustomerRequest request)
        {
            customer.CustomerPreferences = request.PreferenceIds.Select(preference => new CustomerPreference()
            {
                PreferenceId = preference,
                CustomerId = customer.Id
            }).ToList();
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
        }
    }
}
