﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public static class CustomerFactory
    {
        public static Customer CreateFromRequest(CreateOrEditCustomerRequest request)
        {
            var custId = Guid.NewGuid();
            var c = new Customer
            {
                Id = custId,
                CustomerPreferences = request.PreferenceIds.Select(preferenceId => new CustomerPreference()
                {
                    CustomerId = custId,
                    PreferenceId = preferenceId,
                }).ToList(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };
            return c;
        }
    }
}
