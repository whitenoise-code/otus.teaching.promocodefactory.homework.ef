﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public static class HostExtensions
    {
        public static void MigrateDbContext<TContext>(this IHost host, Action<TContext, IServiceProvider> seeder)
            where TContext : DbContext
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<TContext>();

            context.Database.EnsureDeleted();
            context.Database.Migrate();
            seeder(context, services);
        }
    }
}
