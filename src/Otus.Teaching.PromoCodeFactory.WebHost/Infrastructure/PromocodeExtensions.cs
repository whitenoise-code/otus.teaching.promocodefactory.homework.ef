﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public static class PromocodeExtensions
    {
        public static PromoCodeShortResponse ToShortPromocodeResponse(this Promocode p)
        {
            return new PromoCodeShortResponse
            {
                Id = p.Id,
                BeginDate = p.BeginDate.ToString("yyyy-MM-dd"),
                Code = p.PromoCode,
                EndDate = p.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            };
        }

    }
}
